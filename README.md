
### What is this repository for? ###

This project provides a API to use the org.meliorbis.numerics.Numerics library from Scala.

Licensed under the MIT License (http://opensource.org/licenses/MIT). The license file is in src/main/resources.