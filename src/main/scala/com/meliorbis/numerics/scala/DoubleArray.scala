package com.meliorbis.numerics.scala

import scala.language.existentials
import scala.language.implicitConversions
import scala.collection.mutable.ListBuffer
import scala.collection.AbstractIterator
import java.util.function.Consumer
import java.util.PrimitiveIterator
import com.meliorbis.numerics.DoubleNumerics
import com.meliorbis.numerics.NumericsException
import com.meliorbis.numerics.generic._
import com.meliorbis.numerics.generic.impl._
import com.meliorbis.numerics.generic.primitives._
import com.meliorbis.numerics.generic.primitives.impl.Interpolation._
import scala.collection.immutable.NumericRange
import com.meliorbis.numerics.DoubleArrayFactories._
import com.meliorbis.utils.Pair
import com.meliorbis.numerics.DoubleArrayFactories
import scala.collection.mutable.ArraySeq
import scala.collection.mutable.WrappedArray

/**
 * Defines implicits to help with using multidimensional arrays effectively
 * in scala
 *
 * @author Tobias Grasl
 */
object DoubleArray {
  
  private val _numerics = DoubleNumerics.instance
  
  /**
   *  Define a recursive DoubleArray type that works in Scala. In principle the way it is done 
   *  in java (DoubleArray[T<:com.meliorbis.numerics.generic.primitives.DoubleArray[T]]) should work, but it does not. When such a type is
   *  used, the return type from the first op is DoubleArray[_], from which in turn _ is returned.
   *  Painful!
   *  
   *  Also this type serves as a notational convenience, hiding the type system from the end user.
   */
  type DoubleArray = com.meliorbis.numerics.generic.primitives.DoubleArray[T] forSome { type T <: com.meliorbis.numerics.generic.primitives.DoubleArray[T] }


  /**
   * The above double array can't be used by methods implementing java interfaces
   * due to a bug in scala; hence this convenience defn.
   */
  type JavaDoubleArray = com.meliorbis.numerics.generic.primitives.DoubleArray[_]
  
  /**
   * As for DoubleArray above
   */
  type DoubleMappable = Mappable[java.lang.Double, T] forSome { type T <: Mappable[java.lang.Double, T] }

  /**
   * As for DoubleArray above
   */
  type DoubleMappableReducible = MappableReducible[java.lang.Double, T, _] forSome { type T <: MappableReducible[java.lang.Double, T, java.lang.Double] }

  /**
   * Converts a two-element Scala tuple to a Java Pair
   */
  implicit def toPair[A,B]( tuple: (A,B) ) = new Pair[A,B](tuple._1,tuple._2)

  /**
   * Converts any old DoubleArray[_] to DoubleArray. Not sure this bites too often, but worth a try
   */
  implicit def toDoubleArray(arr: com.meliorbis.numerics.generic.primitives.DoubleArray[_]): DoubleArray = arr.asInstanceOf[DoubleArray]

    /**
   * Converts any old DoubleArray[_] to DoubleArray. Not sure this bites too often, but worth a try
   */
  implicit def toDoubleArrayFromT(arr: T forSome { type T <: com.meliorbis.numerics.generic.primitives.DoubleArray[T]}): DoubleArray = arr.asInstanceOf[DoubleArray]

  /**
   *  A double constant to signify 'no selection' in a given dimension
   */
  val $ = -1d;
  
  /**
   * Allow double ranges to be used as Double arrays to fill DoubleArrays!
   */
  implicit def doubleRangeToDoubleArray(range : NumericRange[Double]) : Array[Double] = {
    range.toArray[Double]
  }
  
   /**
   * Allow int ranges to be used as Double arrays to fill DoubleArrays!
   */
 implicit def intRangeToDoubleArray(range : scala.Range) : Array[Double] = {
    range.map( _.asInstanceOf[Double]).toArray[Double]
  }

  /**
   * PrimitiveIterator.OfDouble to Iterator[Double] 
   */
  implicit def asScalaDoubleIterator(iter : PrimitiveIterator.OfDouble) : Iterator[Double] = {
    new AbstractIterator[Double] {
      override def hasNext: Boolean = iter.hasNext()
      
      override def next: Double = iter.nextDouble()
    }
  }
  
  /**
   * Iterator[Double] to PrimitiveIterator.OfDouble
   */
  implicit def asJavaDoubleIterator(iter : Iterator[Double]) : PrimitiveIterator.OfDouble = {
    new PrimitiveIterator.OfDouble {
      override def nextDouble : Double = {
        iter.next
      }
      
      override def hasNext = iter.hasNext
    }
  }
  
  /**
   * Array[Int] => Iterator[Double] to {@link com.meliorbis.numerics.generic.primitives.impl.Interpolation#Discontinuities}
   */
  implicit def asDiscontinuities(fn: Array[Int] => Iterator[Double]) : Discontinuities = {
    new Discontinuities {
      override def getDiscontinuitiesAt(idx: Array[Int]) : PrimitiveIterator.OfDouble = {
        fn(idx)
      }
    }
  } 
  
  /**
   * Converts a closure accepting an array of doubles and returning a double
   * to a DoubleNaryOp
   */
  implicit def toReduction(fn: Iterator[Double] => Double) =
    new DoubleReduction[RuntimeException] {

      override def perform(iter: DoubleSettableIterator): Double = fn( asScalaDoubleIterator(iter.asInstanceOf[PrimitiveIterator.OfDouble]) );
    }
  
  /**
   * Converts a closure accepting an array of doubles and returning a double
   * to a DoubleNaryOp
   */
  implicit def toSplitReduction(fn: DoubleSubspaceSplitIterator => Double) =
    new IndexedReduction[java.lang.Double, RuntimeException] {

      override def perform(iter: SubSpaceSplitIterator[java.lang.Double]): java.lang.Double = fn(iter.asInstanceOf[DoubleSubspaceSplitIterator])
    }

  /**
   * Converts a closure accepting an array of doubles and returning a double
   * to a DoubleNaryOp
   */
  implicit def toNAryOp(fn: (Array[Double]) => Double) =
    new DoubleNaryOp[RuntimeException] {
      def perform(a: Double*): Double = fn(a.toArray)
    }

  /**
   * Converts a closure accepting and returning an array of doubles to a DoubleNaryOp
   */
  def toUnaryMultiOp(fn: Double => Array[Double]) =
    new DoubleMultiValuedNaryOp[RuntimeException] {
      def perform(a: Double*): Array[Double] = fn(a(0))
    }
  
  /**
   * Converts a closure accepting and returning an array of doubles to a DoubleNaryOp
   */
  def toNAryMultiOp(fn: (Array[Double]) => Array[Double]) =
    new DoubleMultiValuedNaryOp[RuntimeException] {
      def perform(a: Double*): Array[Double] = fn(a.toArray)
    }

  /**
   * Converts  6-parameter double closure to an nary op
   */
  implicit def toSenaryArrayOp(fn: (Double, Double, Double, Double, Double, Double) => Double) = 
    new DoubleNaryOp[RuntimeException] {
      def perform(a: Double*): Double = fn(a(0),a(1),a(2),a(3),a(4),a(5))
    }

  /**
   * Converts  5-parameter double closure to an nary op
   */
  implicit def toQuinternaryArrayOp(fn: (Double, Double, Double, Double, Double) => Double) = (arrayIn: Array[Double]) => {
    val Array(a, b, c, d, e) = arrayIn
    fn(a, b, c, d, e)
  }
  
  /**
   * Converts  4-parameter double closure to an nary op
   */
  implicit def toQuaternaryMultiOp(fn: (Double, Double, Double, Double) => Array[Double]) = 
     new DoubleMultiValuedNaryOp[RuntimeException] {
      def perform(ins: Double*): Array[Double] = {
        
        if (ins.length != 4) throw new NumericsException("This operation expects 3 parameters. but " + ins.length + " were passed");
        
        fn(ins(0), ins(1), ins(2), ins(4))
      }
    }

  /**
   * Converts  4-parameter double closure to an nary op
   */
  implicit def toQuaternaryArrayOp(fn: (Double, Double, Double, Double) => Double) = (arrayIn: Array[Double]) => {
    val Array(a, b, c, d) = arrayIn
    fn(a, b, c, d)
  }

  /**
   * Converts 3-parameter double closure to an nary op
   */
  def toTernaryMultiOp(fn: (Double, Double, Double) => Array[Double]) =
    new DoubleMultiValuedNaryOp[RuntimeException] {
      def perform(ins: Double*): Array[Double] = {
        
        if (ins.length != 3) throw new NumericsException("This operation expects 3 parameters. but " + ins.length + " were passed");
        
        fn(ins(0), ins(1), ins(2))
      }
    }
  
  /**
   * Converts 3-parameter double closure to an nary op
   */
  def toTernaryOp(fn: (Double, Double, Double) => Double) =
    new DoubleNaryOp[RuntimeException] {
      def perform(ins: Double*): Double = {
        if (ins.length != 3) throw new NumericsException("This operation expects 3 parameters. but " + ins.length + " were passed");
        fn(ins(0), ins(1), ins(2))
      }
    }

  /**
   * Converts a closure accepting an two doubles and returning a double
   * to a DoubleBinaryOp
   */
  implicit def toBinaryOp(fn: (Double, Double) => Double) =
    new DoubleBinaryOp[RuntimeException] {
      def perform(a: Double, b: Double): Double = fn(a, b)
    }
  
   /**
   * Converts a closure accepting an two doubles and returning a double
   * to a DoubleBinaryOp
   */
  def toBinaryMultiOp(fn: (Double, Double) => Array[Double]) =
    new DoubleMultiValuedNaryOp[RuntimeException] {
      def perform(a: Double*): Array[Double] = fn(a(0), a(1))
    }

  /**
   * Converts a closure accepting a double and returning a double
   * to a DoubleUnaryOp
   */
  implicit def toUnaryOp(fn: (Double) => Double) =
    new DoubleUnaryOp[RuntimeException] {
      def perform(a: Double): Double = fn(a)
    }

  /**
   * Converts a closure accepting a double and returning a double
   * to a DoubleUnaryOp
   */
  implicit def consumerConv(fn: (DoubleArray) => Unit) =
    new Consumer[DoubleArray] {
      def accept(x: DoubleArray): Unit = fn(x)
    }

  /**
   * This implicit class defines the operators that can be used in scala for basic operations on
   * multi-dimensional arrays. It is generic an can therfore only define those operations that can be
   * defined generically
   */
  implicit class ScalaDoubleWithArrayOps[T <: com.meliorbis.numerics.generic.primitives.DoubleArray[T]](m: Double) {

    def +(n: com.meliorbis.numerics.generic.primitives.DoubleArray[T]): DoubleArray = {
      n.add(m)
    }

    def -(n: com.meliorbis.numerics.generic.primitives.DoubleArray[T]): DoubleArray = {
      n.map((x: Double) => m - x)
    }

    def *(n: com.meliorbis.numerics.generic.primitives.DoubleArray[T]): DoubleArray = {
      n.multiply(m)
    }

    def /(n: com.meliorbis.numerics.generic.primitives.DoubleArray[T]): DoubleArray = {
      n.map((x: Double) => m / x)
    }

    def ^(n: Double): Double = {
      Math.pow(m, n)
    }
    
    def ~(steps: Int) : (Double, Int) = {
      (m, steps)
    }
    
    def to(endAndSteps: (Double, Int)) : NumericRange[Double] = {
      val (end, steps) = endAndSteps
      
      m.to(end).by((end-m)/steps)
    }
  }

  /**
   * This class is used to collect multiple DoubleArray instances together and
   * perform operations on them.
   *
   * Rather than using the inbuilt Scala list, wrap it for better control.
   */
  protected abstract class ListOfArrays[T <: ListOfArrays[_]](protected val wrapped: ListBuffer[DoubleMappable]) {

    def this(arrays: List[DoubleMappable]) = {
      this(new ListBuffer[DoubleMappable])
      wrapped.appendAll(arrays)
    }

    /**
     * Performs the mapping provided on the arrays contained in this instance.
     *
     * @param op The operation to be performed, point by point, on the arrays in question
     *
     * @return The result of the operation, which is a new array
     */
    protected def mapOp(op: DoubleNaryOp[_]): DoubleArray =
      wrapped.head.`with`(typedOtherArray(): _*).map(op).asInstanceOf[DoubleArray]

    /**
     * Performs the mapping provided on the arrays contained in this instance, placing the result in the
     * first array in this list, and returning it
     *
     * @param op The operation to be performed, point by point, on the arrays in question
     *
     * @return The result of the operation, which is the first array in this list
     */
    protected def mapModOp(op: DoubleNaryOp[_]): DoubleArray =
      wrapped.head.asInstanceOf[DoubleMappableReducible].modifying().`with`(typedOtherArray(): _*).map(op).nonModifying().asInstanceOf[DoubleArray]

    def multiMap(op: DoubleMultiValuedNaryOp[_]): Array[DoubleArray] = {

      wrapped.head.`with`(typedOtherArray(): _*).map(op).asInstanceOf[Array[DoubleArray]]

    }
     
    def ->(fn: ((Array[Double]) => Double)): DoubleArray = {
      mapOp(toNAryOp(fn))
    }
    
    def :->(fn: ((Array[Double]) => Array[Double])): Array[DoubleArray] = {
      multiMap(toNAryMultiOp(fn))
    }

    /* There is no equivalent to java's toArray yielding a specific type, so this is the
     * equivalent to get an array of the tail for passing to the head array's with
     */
    private def typedOtherArray(): Array[MultiDimensionalArray[java.lang.Double, _]] = {

      val array = new Array[MultiDimensionalArray[java.lang.Double, _]](wrapped.size - 1)

      for (i <- 1 until wrapped.size) {
        array(i - 1) = wrapped(i).asInstanceOf[MultiDimensionalArray[java.lang.Double, _]]
      }

      array
    }
    
    def ::(other: DoubleMappable) : T
    
    def toList = wrapped.toList
  }

  class TwoArrays(a: DoubleMappable, b: DoubleMappable) extends ListOfArrays[ThreeArrays](new ListBuffer[DoubleMappable]() += a += b) {

    def map(fn: ((Double, Double) => Double)): DoubleArray = {
      mapOp(toBinaryOp(fn))
    }
    
    def ->(fn: ((Double, Double) => Double)): DoubleArray = {
      map(fn)
    }
    
    def :->(fn: ((Double, Double) => Array[Double])): Array[DoubleArray] = {
      multiMap(toBinaryMultiOp(fn))
    }
    
    def mapMod(fn: ((Double, Double) => Double)): DoubleArray = {
      mapModOp(toBinaryOp(fn))
    }

    def =->(fn: ((Double, Double) => Double)): DoubleArray = {
      mapMod(fn)
    }


    def ::(other: DoubleMappable) = new ThreeArrays(other +=: this.wrapped)
  }

  class ThreeArrays(wrapped: ListBuffer[DoubleMappable]) extends ListOfArrays[NArrays](wrapped) {

    def map(fn: ((Double, Double, Double) => Double)): DoubleArray = {
      mapOp(toTernaryOp(fn))
    }

    def ->(fn: ((Double, Double, Double) => Double)): DoubleArray = {
      map(fn)
    }
    
    def :->(fn: ((Double, Double, Double) => Array[Double])): Array[DoubleArray] = {
      multiMap(toTernaryMultiOp(fn))
    }

    def mapMod(fn: ((Double, Double, Double) => Double)): DoubleArray = {
      mapModOp(toTernaryOp(fn))
    }

    def =->(fn: ((Double, Double, Double) => Double)): DoubleArray = {
      mapMod(fn)
    }

    def ::(other: DoubleMappable) = new NArrays(other +=: this.wrapped)
  }

  class NArrays(wrapped: ListBuffer[DoubleMappable]) extends ListOfArrays[NArrays](wrapped) {

    def this() = this(ListBuffer[DoubleMappable]())
    
    def map(fn: (Array[Double] => Double)): DoubleArray = {
      mapOp(toNAryOp(fn))
    }

    def mapMod(fn: (Array[Double] => Double)): DoubleArray = {
      mapModOp(toNAryOp(fn))
    }

    def =->(fn: (Array[Double] => Double)): DoubleArray = {
      mapMod(fn)
    }

    def ::(another: DoubleMappable) = new NArrays(another +=: this.wrapped)

  }
  
  var EMPTY: ListOfArrays[NArrays] = new NArrays(ListBuffer[DoubleMappable]())
  

  /**
   * This implicit class defines the operators that can be used in scala for basic operations on
   * multi-dimensional arrays. It is generic an can therfore only define those operations that can be
   * defined generically
   */
  implicit class ScalaDoubleArray(m: DoubleArray) extends ScalaMappableReducible(m) {

    def central = m(m.numberOfElements/2)
    
    def \(d: Int*) = {
      
      if(d.length < 1) {
        throw new MultiDimensionalArrayException("'\\' requires at least one dimension")
      }
      
      m.across(d:_*)
    }
    
    /* Addition Operators
     */
    def +=(n: Double): DoubleArray = {
      m.modifying().add(n).nonModifying().asInstanceOf[DoubleArray]
    }

    def +(n: Double): DoubleArray = {
      return m.add(n)
    }

    /* Subtraction Operators
     */
    def -=(n: Double): DoubleArray = {
      m.modifying().subtract(n).nonModifying().asInstanceOf[DoubleArray]
    }

    def -(n: Double): DoubleArray = {
      return m.subtract(n)
    }

    /* Multiplication Operators
     */
    def *=(n: Double): DoubleArray = {
      m.modifying().multiply(n).nonModifying().asInstanceOf[DoubleArray]
    }

    def *(n: Double): DoubleArray = {
      return m.multiply(n)
    }

    /* Division Operators
     */
    def /=(n: Double): DoubleArray = {
      m.modifying().divide(n).nonModifying().asInstanceOf[DoubleArray]
    }

    def /(n: Double): DoubleArray = {
      return m.divide(n)
    }

    /* Scalar Power
     */
    def ^(n: Double): DoubleArray = {
      m -> (_ ^ n)
    }

    /* Matrix multiplication
     */
    def %*(n: DoubleArray): DoubleArray = {
      return m.matrixMultiply(n)
    }
    
    /* Matrix transpose
     */
    def t(): DoubleArray = {
      return m.transpose(0,1)
    }

    /* Array style Getter!
     */
    def apply(index: Array[Int]): Double = {
      return m.get(index: _*)
    }

    /* Array style Getter!
     */
    def apply(index: Int*): Double = {
      return m.get(index: _*)
    }

    /* Array style subarray!!
    */
    def apply(index: Double*): DoubleArray = {
      return m.at((index map (idx => idx.toInt)): _*)
    }

    /* Operators for filling
     */
    override def <<(values: Array[Double]): DoubleArray = {
      m.fill(values: _*)
    }

    override def <<(values: Double*): DoubleArray = {
      m.fill(values: _*)
    }
    
    override def <<(values: DoubleArray) = {
      m.fill(values)
    }

    def update(index: Array[Int], value: Double) = {
      m.set(value, index: _*)
    }

    /**
     * Need to create update functions for any number of indexes
     */
    def update(index1: Int, value: Double) = {
      m.set(value, index1)
    }

    def update(index1: Int, index2: Int, value: Double) = {
      m.set(value, index1, index2)
    }

    def update(index1: Int, index2: Int, index3: Int, value: Double) = {
      m.set(value, index1, index2, index3)
    }

    def update(index1: Int, index2: Int, index3: Int, index4: Int, value: Double) = {
      m.set(value, index1, index2, index3, index4)
    }

    def update(index1: Int, index2: Int, index3: Int, index4: Int, index5: Int, value: Double) = {
      m.set(value, index1, index2, index3, index4, index5)
    }

    def update(index1: Int, index2: Int, index3: Int, index4: Int, index5: Int,
      index6: Int, value: Double) = {
      m.set(value, index1, index2, index3, index4, index5, index6)
    }

    def update(index1: Int, index2: Int, index3: Int, index4: Int, index5: Int,
      index6: Int, index7: Int, value: Double) = {
      m.set(value, index1, index2, index3, index4, index5, index6, index7)
    }

    def update(index1: Int, index2: Int, index3: Int, index4: Int, index5: Int,
      index6: Int, index7: Int, index8: Int, value: Double) = {
      m.set(value, index1, index2, index3, index4, index5, index6, index7, index8)
    }

    def update(index1: Int, index2: Int, index3: Int, index4: Int, index5: Int,
      index6: Int, index7: Int, index8: Int, index9: Int, value: Double) = {
      m.set(value, index1, index2, index3, index4, index5, index6, index7, index8,
        index9)
    }

    def update(index1: Int, index2: Int, index3: Int, index4: Int, index5: Int,
      index6: Int, index7: Int, index8: Int, index9: Int, index10: Int,
      value: Double) = {
      m.set(value, index1, index2, index3, index4, index5, index6, index7, index8,
        index9, index10)
    }

    def each(op: Double => Double) = {
      m.map(new DoubleUnaryOp[RuntimeException] {
        override def perform(input_ : Double): Double = {
          return op(input_)
        }
      })
    }
  }

  /**
   * This implicit class wraps results or across to provide arithmetic operations
   *
   * @param m
   * @param T
   */
  implicit class ScalaMappableReducible(m: DoubleMappableReducible) {

    def ::(other: DoubleMappableReducible): TwoArrays = new TwoArrays(other, m)

    def ->=(fn: Double => Double): DoubleArray = {
      m.modifying().map(toUnaryOp(fn))

      m.asInstanceOf[DoubleArray]
    }

    def +=(n: DoubleArray): DoubleArray = {
      m.modifying().add(n).nonModifying().asInstanceOf[DoubleArray]
    }

    def +(n: DoubleArray): DoubleArray = {
      return m.add(n).asInstanceOf[DoubleArray]
    }

    def -=(n: DoubleArray): Unit = {
      m.modifying().subtract(n)
    }

    def -(n: DoubleArray): DoubleArray = {
      return m.subtract(n).asInstanceOf[DoubleArray]
    }

    def *=(n: DoubleArray): Unit = {
      m.modifying().multiply(n)
    }

    def *(n: DoubleArray): DoubleArray = {
      return m.multiply(n).asInstanceOf[DoubleArray]
    }

    def /=(n: DoubleArray): Unit = {
      m.modifying().divide(n)
    }

    def /(n: DoubleArray): DoubleArray = {
      return m.divide(n).asInstanceOf[DoubleArray]
    }
    
    def <<(values: DoubleArray) = {
      (m::ScalaMappableReducible(values)) =-> ((a,b) => b)
    }
    
    def <<(values: Array[Double]): DoubleArray = {
      m << (createArrayOfSize(values.length)<<values)
    }

    def <<(values: Double*): DoubleArray = {
      m << values.toArray
    }
    
    
  }

  implicit class ScalaMappable(m: DoubleMappable) {

    def ~(other: DoubleMappable) = new TwoArrays(m, other)

    def map(fn: Double => Double): DoubleArray = {
      var res = m.map(toUnaryOp(fn))

      res.asInstanceOf[DoubleMappableReducible].nonModifying.asInstanceOf[DoubleArray]
    }

    def ->(fn: Double => Double): DoubleArray = {
      map(fn)
    }
    
    def multiMap(op: DoubleMultiValuedNaryOp[_]): Array[DoubleArray] = {
      m.map(op).asInstanceOf[Array[DoubleArray]]
    }

    def :->(fn: Double => Array[Double]): Array[DoubleArray] = {
      multiMap(toUnaryMultiOp(fn))
    }
  }

  /**
   * When a list of DoubleArray instances is provided but a single array is required, then (by assumption) the list
   * is actually to be combined into one array with the extra dimension along the list. This method performs that
   * transformation
   *
   * @return A DoubleArray with one extra dimension as each array in the list, of the same size as the list, and with
   * the arrays layered on top of each other in that dimension
   */
  
  implicit def toList(m: ListOfArrays[_]) = m.toList
  implicit def listToStackedArray(m: ListOfArrays[_]) = m.head.asInstanceOf[DoubleArray].stack(
      m.tail.asInstanceOf[List[MultiDimensionalArray[_ <: java.lang.Double, _]]].toArray: _*)

  implicit def listToStackedArray(m: ListBuffer[DoubleArray]) = m.head.asInstanceOf[DoubleArray].stack(
      m.tail.asInstanceOf[List[MultiDimensionalArray[_ <: java.lang.Double, _]]].toArray: _*)
}