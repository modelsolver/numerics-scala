package com.meliorbis.numerics.scala

import com.meliorbis.numerics.scala.DoubleArray._
import org.scalatest.matchers._
import org.scalautils.Equality
import org.apache.commons.math3.util.Precision



/**
 * @author toby
 */
object Matchers {
  
  def equalArray(arr: DoubleArray, precision: Double) : Matcher[DoubleArray] = {
    
    return new Matcher[DoubleArray](){
      override def apply(value: DoubleArray) = {
        var matches = true
        
        arr~value->((a: Double, b: Double) => {
          matches = matches & Precision.equals(a,b,precision)
          0d
        })
        
        MatchResult(matches,
            s"""Array $value does not equal $arr""",
            s"""Array $value equals $arr""")
      }
    }
  }
  
}