package com.meliorbis.numerics.scala

import org.junit.Assert._
import org.junit.Test
import org.scalatest.junit.AssertionsForJUnit
import com.meliorbis.numerics.DoubleNumerics
import com.meliorbis.numerics.test.ArrayAssert
import DoubleArray._
import com.meliorbis.numerics.DoubleArrayFactories._

/**
 * Tests the array library
 */
class ArrayTest extends AssertionsForJUnit {

  val ALLOWED_ERROR: Double = 1e-15;
  
//  @Test def testFillRange() {
//    
//    var array = createArrayOfSize(5) << (1 to 5)
//    
//    // Check each value is its index plus 1
//    (0 to 4).foreach( x => assertEquals(x + 1, array(x) , ALLOWED_ERROR) )
//
//    val array2 = createArrayOfSize(200) << (0d to 2.5 :: 199)
//    
//    val expectedStep = 2.5/199
//    
//    (0 to 200).foreach( idx => assertEquals(idx * expectedStep, array2(idx), ALLOWED_ERROR))
//  }
  
  @Test def testBasics() {
    var array = createArrayOfSize(2,3,5)
    
    var size = array.size()
    
    assertEquals(3, size.length)
    assertEquals(30, array.numberOfElements())
    
    array = createArrayOfSize(2,2)
    
    array << Array(1d,2,3,4)
    
    // Check that the value was correctly written, and can be retrieved
    assertEquals(2, array(0,1), ALLOWED_ERROR)
    
    // Select the second row
    var subArray = array(1,$)
    
    assertEquals(2,subArray.numberOfElements())
    assertEquals(3, subArray(0),ALLOWED_ERROR)
    assertEquals(4, subArray(1),ALLOWED_ERROR)
    
    
    subArray(1) = 1.1
    
    // Assignment works
    assertEquals(1.1, subArray(1),ALLOWED_ERROR)
    
    // Assigning to the subarray also changes the parent!
    assertEquals(1.1, array(1,1),ALLOWED_ERROR)

    array = createArrayOfSize(2,2)
    val filler = createArrayOfSize(2)
    filler << Array(1d,2)

    // Filling across
    array.across(1) << filler


    assertEquals(1d,array(0),ALLOWED_ERROR)
    assertEquals(2d,array(1),ALLOWED_ERROR)
    assertEquals(1d,array(2),ALLOWED_ERROR)
    assertEquals(2d,array(3),ALLOWED_ERROR)
  }
  
  @Test def testUntil()
  {
    val array = createArrayOfSize(2,2)
    
    array << (1 to 4)
    
    assertEquals(1d,array(0,0),ALLOWED_ERROR)
    assertEquals(2d,array(0,1),ALLOWED_ERROR)
    assertEquals(3d,array(1,0),ALLOWED_ERROR)
    assertEquals(4d,array(1,1),ALLOWED_ERROR)
  }
  
  @Test def testArithmeticOps()
  {
    val array1 = createArrayOfSize(2, 2)
    val array2 = createArrayOfSize(2, 2)

    array1 << Array(.1,.2,.3,.4)
    array2 << Array(1d, 2, 3, 4)

    
    val array3 = array1 * array2
    
    assertEquals(4, array3.numberOfElements())
    
    // Linear indexing!
    assertEquals(.9, array3(2), ALLOWED_ERROR)
    
    array3 += 1d
    
    assertEquals(1.9, array3(1,0), ALLOWED_ERROR)
    
    array3 += array2

    assertEquals(4.9, array3(1,0), ALLOWED_ERROR)
    
    array3 /= 2d

    assertEquals(2.45, array3(1,0), ALLOWED_ERROR)
    
    array3 /= array2

    assertEquals(2.45/3d, array3(1,0), ALLOWED_ERROR)

    array3 -= array1

    assertEquals(2.45/3d-.3, array3(1,0), ALLOWED_ERROR)

    var result = 5d + array3

    assertEquals(2.45/3d+4.7, result(1,0), ALLOWED_ERROR)

    val array4 = array3 - 2

    // array3 should be unchanged
    assertEquals(2.45/3d-.3, array3(1,0), ALLOWED_ERROR)
    
    // array4 should contain the change
    assertEquals(2.45/3d-2.3, array4(1,0), ALLOWED_ERROR)

    result = 5d/array2

    assertEquals(5, result(0), ALLOWED_ERROR)
    assertEquals(5d/2, result(1), ALLOWED_ERROR)
    assertEquals(5d/3, result(2), ALLOWED_ERROR)
    assertEquals(5d/4, result(3), ALLOWED_ERROR)

    result = 2d*array2

    assertEquals(2, result(0), ALLOWED_ERROR)
    assertEquals(4, result(1), ALLOWED_ERROR)
    assertEquals(6, result(2), ALLOWED_ERROR)
    assertEquals(8, result(3), ALLOWED_ERROR)

    result = 10d - array2

    assertEquals(9, result(0), ALLOWED_ERROR)
    assertEquals(8, result(1), ALLOWED_ERROR)
    assertEquals(7, result(2), ALLOWED_ERROR)
    assertEquals(6, result(3), ALLOWED_ERROR)
  }

  @Test def testArithmeticAcrossOps()
  {
    val array1 = createArrayOfSize(2, 2)
    val array2 = createArrayOfSize(2)

    array1 << Array(.1,.2,.3,.4)
    array2 << Array(2d, 3)


    array1.across(1) += array2

    assertEquals(2.1,array1(0),ALLOWED_ERROR)
    assertEquals(3.2,array1(1),ALLOWED_ERROR)
    assertEquals(2.3,array1(2),ALLOWED_ERROR)
    assertEquals(3.4,array1(3),ALLOWED_ERROR)

  }
  
  @Test def testNaryOps()
  {
    val array1 = createArrayOfSize(2, 2)
    val array2 = createArrayOfSize(2, 2)

    array1 << Array(.1,.2,.3,.4)
    array2 << Array(1d, 2, 3, 4)
    
    var array3 : DoubleArray = array1 -> (Math.exp _)
    
    assertEquals(4, array3.numberOfElements())
    assertEquals(Math.exp(.2), array3(0,1), ALLOWED_ERROR)
    
    array3 = (array1 :: array2) -> ( _ + 2 * _ )
    
    assertEquals(6.3, array3(1,0), ALLOWED_ERROR)
  }
  
  @Test def testModifying()
  {
    val array1 = createArrayOfSize(2, 2)
    val array2 = createArrayOfSize(2, 2)

    array1 << (.1,.2,.3,.4)
    array2 << (1d, 2, 3, 4)
    
    array1 ->= (Math.exp _)
    
    assertEquals(Math.exp(.2), array1(0,1), ALLOWED_ERROR)
    assertEquals(Math.exp(.3), array1(1,0), ALLOWED_ERROR)
    
    (array2 :: array1) =-> ( 2 * _ +  _ )
    
    assertEquals(6 + Math.exp(.3), array2(1,0), ALLOWED_ERROR)
    assertEquals(8 + Math.exp(.4), array2(1,1), ALLOWED_ERROR)
  }

  
  @Test def testList()
  {
     val array1 = createArrayOfSize(2, 2)
     val array2 = createArrayOfSize(2, 2)
     
     array1 += 1d
     array2 += 2d
     
     
     val array3 = createArrayOfSize(2,2)
     
     array3 += 3d
     
     ArrayAssert.assertEquals(array3, (array1 :: array2) -> (_ + _), ALLOWED_ERROR)
         
     var result = (array1 :: array2 :: array3) -> ( _ + _ * _ )
     
     val array4 = createArrayOfSize(2,2)
     array4 += 7
     
     ArrayAssert.assertEquals(array4, result,ALLOWED_ERROR)
     
     /* MODIFYING should affect the first array
      * 
      * NOTE: non-affectedness of other arrays is implicitly tested in further actions below
      */
     (array1 :: array2 :: array3) =-> ( _ * _ * _ )
     
     assertEquals(6d,array1(0),ALLOWED_ERROR)
 
     (array1 :: array2 :: array3 :: array4) =-> ((a: Double, b: Double, c: Double, d: Double) => (a + b)/(c+d))
     
     val checker = createArrayOfSize(2,2)
     checker += .8
     
     ArrayAssert.assertEquals(checker,array1,ALLOWED_ERROR)
 
     result = (array1 :: array2 :: array3 :: array4) -> (in => {
       val Array(a, b, c, d) = in
       (a^b) * (c + d)
     })
     
     checker << 6.4
     
     ArrayAssert.assertEquals(checker,result,ALLOWED_ERROR)
  }
  
  @Test def testAcrossList()
  {
     val array1 = createArrayOfSize(2, 2)
     
     array1 += 2d

     val array2 = createArrayOfSize(2)
     val array3 = createArrayOfSize(2)
     
     array2 << (2d,3d)
     array3 << (7d,5d)
     
     val result = (array1.across(1) :: array2).map(_ * _)
     
     assertArrayEquals(Array[Int](2,2), result.size)
    
     val checker = createArrayOfSize(2, 2)
     
     checker << (4, 6, 4, 6)
     
      ArrayAssert.assertEquals(checker, result, ALLOWED_ERROR)
      
      /* MODIFYING Across operation - Bonza!
       */
      array1\(0) :: array2 :: array3 =-> ( (a, b, c) => (a + b) * c )
      
      checker << (28,28,25,25)

      ArrayAssert.assertEquals(checker, array1, ALLOWED_ERROR)

      array1.across(1) -= array2

      checker << (26,25,23,22)

      ArrayAssert.assertEquals(checker, array1, ALLOWED_ERROR)
  }
  
  @Test def testListMultiMap() {
    
    val a = createArrayOfSize(2, 2)
    val b = createArrayOfSize(2, 2)
     
    a << (1,2,3,4)
    b << (3,4,5,6)
 
    val Array(c, d) = (a::b) :-> (in => {
      val Array(a,b) = in
      
      Array(a+b,a-b)
    })
        
    ArrayAssert.assertEquals(Array(4d,6,8,10), c, ALLOWED_ERROR)
    ArrayAssert.assertEquals(Array(-2d,-2,-2,-2), d, ALLOWED_ERROR)
  }
  
//   @Test def testSingleArrayMultiMap() {
//    
//    val a = createArrayOfSize(2, 2)
//     
//    a << (1,2,3,4)
// 
//    val Array(c, d) = a->( (a : Double) => {
//      
//      Array(2d^a,3d^a)
//    })
//        
//    ArrayAssert.assertEquals(Array(2d,4,8,16), c, ALLOWED_ERROR)
//    ArrayAssert.assertEquals(Array(3d,9,27,81), d, ALLOWED_ERROR)
//  }
}