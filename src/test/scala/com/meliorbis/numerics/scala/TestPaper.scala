package com.meliorbis.numerics.scala

import org.scalatest.junit.AssertionsForJUnit
import org.junit.Test

import com.meliorbis.numerics.DoubleArrayFactories._
import com.meliorbis.numerics.test.ArrayAssert._
import scala.math._
import com.meliorbis.numerics.scala.DoubleArray._
import org.junit.Assert
import com.meliorbis.numerics.generic.primitives.impl.Interpolation._

class TestPaper extends AssertionsForJUnit {

  @Test def testMap() {

    val a = createArrayOfSize(2)
    a << (1, 4)
    val b = a -> (ai => ai * 10 + sqrt(ai)) // Returns (11,42)

    assertEquals(createArray(11, 42), b, 1e-10)

    val c = a ~ b -> ((a, b) => (b - sqrt(a)) / a)

    assertEquals(createArray(10, 10), c, 1e-10)

    val d = a :: b
    //    val Array(d,e) = a~b :-> ((a,b) => Array(floor(b/a),b%a))
    //    
    //    assertEquals(createArray(11,10),d, 1e-10)
    //    
    //    assertEquals(createArray(0,2),e, 1e-10)
  }

  @Test def testMultiMap() {

    val a = createArrayOfSize(2)
    val b = createArrayOfSize(2)
    val c = createArrayOfSize(2)

    a << (1, 2) // Assumes A,B,C are all 2-element DoubleArray
    b << (2, 3)
    c << (3, 5)

    val Array(d, e) = (a :: b :: c) :-> ((ai, bi, ci) => {
      Array((ai + bi) * ci, (ai + bi) / ci)
    })
    assertEquals(createArray(9, 25), d, 1e-10)
    assertEquals(createArray(1, 1), e, 1e-10)

  }
  
  @Test def testMapAcross() {

    val a = createArrayOfSize(2,2) 
    val b = createArrayOfSize(2)
    
    a << (1 to 4) 
    b << (5,6)

    val c = (a\(1) :: b) -> ((ai,bi) => ai + bi)
    
    assertEquals(createArrayOfSize(2,2) << (6,8,8,10), c, 1e-10)

    val d = (c\(0)).reduce( (iter : Iterator[Double]) => {
       iter.reduce( (prod,ci) => prod * ci)  // Product of elements
    })
    
    assertEquals(createArrayOfSize(2) << (48,80), d, 1e-10)
  }  

  @Test def testReduce() {
    val a = createArrayOfSize(10) << (1 to 10)
    
    val res = a.reduce( (it:Iterator[Double]) => {
      var s = 0d
      
      for( i <- it ) s = s+i
      
      s
    })
    
    Assert.assertEquals(10*11/2,round(res))
  }
  
  @Test def testInterp() {
    val a = createArrayOfSize(5,7,3,2)
    val x0 = createArrayOfSize(5) << (1,2,3,4,5)
    val x2 = createArrayOfSize(3) << (3,6,9)
    
    a << (1 to a.numberOfElements) // Fill A with some values 
    val b = interp(a, spec(0,x0,2.5), spec(2,x2,4))  
    
    Assert.assertArrayEquals(Array(7,2),b.size)
  }
  
  @Test def testInterpFunction() {
    val srcX = createArrayOfSize(5,2)
    val srcY = createArrayOfSize(5,2)
    
    srcX\0 << (1 to 5)
    srcY\0 << (2 to (10,2)) // so y = 2x ...
    srcY\1 += createArray(2,3) // ...+ 2(3) along the first (second) row
    
    val targetX = createArrayOfSize(10,2)
    targetX\0 << (5 to (50,5))
    
    val res = interpolateFunction(srcX, srcY, 0, targetX)
    
    val expectedY = targetX * 2
    expectedY\1 += createArray(2,3)
    
    assertEquals(expectedY, res, 1e-10)    
  }
}