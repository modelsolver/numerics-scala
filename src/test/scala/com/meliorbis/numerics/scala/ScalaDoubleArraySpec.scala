package com.meliorbis.numerics.scala

import org.scalatest._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import com.meliorbis.numerics.scala.DoubleArray._
import com.meliorbis.numerics.NumericsException
import com.meliorbis.numerics.generic.MultiDimensionalArrayException
import com.meliorbis.numerics.DoubleArrayFactories._

@RunWith(classOf[JUnitRunner])
class ScalaDoubleArraySpec extends FlatSpec with Matchers {
  
  "createArrayOfSize" should "create arrays of the correct size" in {
    
    val newArray = createArrayOfSize(2,3,4)
    
    newArray.size() should be (Array(2,3,4))
  }
  
  it should "create arrays of 0s" in {
    
    val newArray = createArrayOfSize(5)
    
    newArray(2) should be (0)
  }
  
  "<<" should "fill the array with the provided values" in {
    
    val newArray = createArrayOfSize(2,3)
    
    newArray << (1 to 6)
    
    newArray.toArray should be (1 to 6)
    newArray(1,0) should be (4)
  }
  
  it should "repeatedly fill longer arrays" in {
    
    val newArray = createArrayOfSize(10)
    
    newArray << (1 to 5)
    
    newArray(4) should be (5)
    newArray(5) should be (1)
    newArray(8) should be (4)
   
  }
  
  it should "throw and Exception when the size of the array to be filled " +
                "is not a multiple of the filling array's size" in {
    
    val newArray = createArrayOfSize(11)
    
    a [MultiDimensionalArrayException] should be thrownBy {
      newArray << (1 to 5)
    }
  }
  
  it should "accept DoubleArray as input" in {
    val newArray = createArrayOfSize(5)
    val input = createArrayOfSize(5) << (2 to 6)
    
    newArray << input
    
    newArray(3) should be (5) 
    
    // TODO: Make this pass!
//    val repeatedFill = createArrayOfSize(10)
//    
//    repeatedFill << input
//    
//    repeatedFill(7) should be (5) 
  }
  
  
  "\\" should "apply a mapping across dimensions" in {
    
    val newArray = createArrayOfSize(2,2)
    newArray << (1 to 4)
    
    newArray\(1) *= (createArrayOfSize(2) << (2 to 3))
    
    newArray(0) should be (2)
    newArray(1) should be (6)
    newArray(2) should be (6)
    newArray(3) should be (12)
  }
  
  it should "reject empty dimensions" in {
    
    val newArray = createArrayOfSize(2,2)
    newArray << (1 to 4)
    
    a[MultiDimensionalArrayException] should be thrownBy {
       newArray\() 
    }
  }
  
  it should "reject higher dimensions" in {
    
    val newArray = createArrayOfSize(2,2)
    newArray << (1 to 4)
    
    a[ArrayIndexOutOfBoundsException] should be thrownBy {
       newArray\(2)
    }
  }
  
  /* ::
   */
  "::" should "create a list of arrays" in {
    val newArray = createArrayOfSize(1)
    val twoArray = createArrayOfSize(2)
    
    val res = ( newArray :: twoArray ) 
    toList(res) should have length 2
  }
  
  it should "maintain order" in {
    val newArray = createArrayOfSize(1)
    val twoArray = createArrayOfSize(2)
    
    val res = ( newArray :: twoArray ) 
    res(0) should be (newArray)
    res(1) should be (twoArray)
  }
  
  /* List of arrays
   */
  "List[DoubleArray]" should "implicitly 'stack' to DoubleArray" in {
    val newArray = createArrayOfSize(2)
    val twoArray = createArrayOfSize(2)
    
    newArray << (1 to 2)
    twoArray << (8 to 9)
    
    val thirdArray : DoubleArray = (newArray::twoArray)
    
    // The two arrays, one atop t'other
    thirdArray.size === Array(2,2)
   
    thirdArray(0) should be (1)
    thirdArray(1) should be (8)
    thirdArray(2) should be (2)
    thirdArray(3) should be (9)
  }
  
  "\\() = " should "modify the array" in {
    val newArray = createArrayOfSize(2,2) << (1,2,3,4)
    
    newArray\(0) *= createArray(2,3)
    
    newArray should be (createArrayOfSize(2,2)<<(2,4,9,12))
  }
  
}